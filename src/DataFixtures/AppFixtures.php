<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\ORM\Doctrine\Populator;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $generator = \Faker\Factory::create('fr_FR');
        $populator = new Populator($generator, $manager);

        $populator->addEntity(Category::class, 15);
        $populator->addEntity(Article::class, 300);
        $populator->execute();

        $manager->flush();
    }
}
