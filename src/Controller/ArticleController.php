<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Category;
use App\Form\ArticleType;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class ArticleController
 * @Route("/api")
 */

class ArticleController extends Controller
{
    private function articleNotFound()
    {
        return View::create(['message' => 'Place not found'], Response::HTTP_NOT_FOUND);
    }
    /**
     * @Rest\View(serializerGroups={"category"})
     * @Rest\Get("/articles", name="articles_all")
     * @param Request $request
     * @return Article $articles
     */
    public function getArticlesAction(Request $request)
    {
        $articles = $this->getDoctrine()->getRepository(Article::class)->findAll();
        /**
         * @var $articles Article
         */
        return $articles;
    }

    /**
     * @Rest\View(serializerGroups={"category"})
     * @Rest\Get("/articles/{id}", name="article_one")
     * @param int $id
     * @return Article $article
     */
    public function getArticleAction(int $id)
    {
        $article = $this->getDoctrine()->getRepository(Article::class)->find($id);
        /**
         * @var $article Article
         */
        if(empty($article)) {
            return $this->articleNotFound();
       }
        return $article;
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED, serializerGroups={"category"})
     * @Rest\Post("/categories/{id}/articles", name="post_article")
     * @param Request $request
     * @param int $id
     * @return
     */
    public function postArticlesAction(Request $request, int $id)
    {
        $category = $this->getDoctrine()->getRepository(Category::class)->find($id);
        /**
         * @var $category Category
         */
        if (empty($category)) {
            return $this->articleNotFound();
        }

        $article = new Article();
        $article->setCategory($category);
        $form = $this->createForm(ArticleType::class, $article);
        $form->submit($request->request->all());

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();
            return $article;
        } else {
            return $form;
        }
    }









}