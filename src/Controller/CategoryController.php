<?php

namespace App\Controller;

use App\Entity\Category;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CategoryController
 * @package App\Controller
 * @Route("/api")
 */

class CategoryController extends Controller
{
    /**
     * @Rest\View(serializerGroups={"article"})
     * @Rest\Get("/categories", name="categories_all")
     * @return Category $category
     */
    public function getCategories()
    {
        $categories = $this->getDoctrine()->getRepository(Category::class)->findAll();
        /**
         * @var $categories Category
         */
        return $categories;
    }

    /**
     * @Rest\View(serializerGroups={"article"})
     * @Rest\Get("/categories/{id}")
     * @param int $id
     * @return Category $category
     */
    public function getCategoryAction(int $id)
    {
        $category = $this->getDoctrine()->getRepository(Category::class)->find($id);
        /**
         * @var $category Category
         */
        if(empty($category)) {
            return View::create(['message' => 'Place not found'], Response::HTTP_NOT_FOUND);
        }
        return $category;
    }

}